import React from "react"
import { graphql, useStaticQuery} from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { BgImage} from "gbimage-bridge"
import ButtonAprende from "../Button"
import { Link } from "gatsby";

const Hero = () => {
    const {backgroundImage123} = useStaticQuery (
        graphql`
        query {
            backgroundImage123: file(relativePath: {eq: "telesan_gracias.jpg"}) {
                childImageSharp {
                  gatsbyImageData(
                    width: 2000, 
                    quality: 50, 
                    webpOptions: {quality: 70})
                }
            }
        }
    `
    )

    const pluginImage = getImage(backgroundImage123)
    return (
       <BgImage image ={pluginImage} className="masthead">
           <div className = "heroboxtext">
                <h1>¡Gracias por comunicarte con nosotros!</h1>
                <div className = "heroboxtext">
                     <h2>Pronto te contáctaremos</h2>
                </div>
                <Link to="/">
                    <ButtonAprende label="Regresar"/>
                </Link>
           </div>
       </BgImage>
    )

}

export default Hero