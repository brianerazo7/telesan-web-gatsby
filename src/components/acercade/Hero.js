import React from "react"
import { graphql, useStaticQuery} from "gatsby"
import { getImage } from "gatsby-plugin-image"
import { BgImage} from "gbimage-bridge"

const Hero = () => {
    const {backgroundImage123} = useStaticQuery (
        graphql`
        query {
            backgroundImage123: file(relativePath: {eq: "acercade_hero_telesan.jpg"}) {
                childImageSharp {
                  gatsbyImageData(
                    width: 2000, 
                    quality: 50, 
                    webpOptions: {quality: 70})
                }
            }
        }
    `
    )

    const pluginImage = getImage(backgroundImage123)
    return (
       <BgImage image ={pluginImage} className="masthead">
           <div className = "heroboxtext">
                <h1>Sobre Nosotros</h1>
           </div>
       </BgImage>
    )

}

export default Hero