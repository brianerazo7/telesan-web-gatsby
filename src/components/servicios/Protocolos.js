import React from "react"
import { StaticImage } from "gatsby-plugin-image"

const Objetivos = () => {
    return (
    <section id = "telesan-hero2">
        <div className = "content-telesan">
            <div className= "row">
                <div className = "columns grafico">
                    <StaticImage
                        src = "../../assets/sobrenosotros/telesan_objetivos.jpg"
                        width = {700}
                        alt= "Telesan teleconsulta"
                    />]
                </div>
                <div className = "column">
                    <h1>Productos de protocololización</h1>
                    <p>
                        <ul>
                            <li>Protocolo general de <em>TeleSalud</em>.</li>
                            <li> Protocolo de atención nutricional.</li>
                            <li> Guía de teleaducación.</li>
                            <li> Guía Ética.</li>
                            <li> Manual de cultura alimentaria GAD</li>

                        </ul>
                    </p>
                 </div>
            </div>
        </div>
    </section>
    )
}

export default Objetivos